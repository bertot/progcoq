#!/usr/bin/perl

open(Main, '<', "short_article.tex");
open(Bio, '<', "bertot_bio.tex");
open(Intro, '<', "intro_prog_fun.tex");
open(Transfer, '<', "abstract_transfer.tex");
while(<Main>){
  if(/\\input bertot_bio.tex/){
     while(<Bio>){ print; }
  } elsif (/\\input intro_prog_fun.tex/) {
    while(<Intro>) { print; }
  } elsif (/\\input abstract_transfer.tex/) {
    while(<Transfer>) { print; }
  } else {
    print;
  }
}
