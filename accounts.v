Require Import String List ZArith Bool.
Require Import FMaps FMapAVL Int.

Open Scope Z_scope.

Section abstract_program.

Variable accounts : Type.

Variable empty : accounts.

Variable set : string -> Z -> accounts -> accounts.

Variable get : string -> accounts -> Z.

Hypothesis get_after_set_same_key :
  forall m k v, get k (set k v m) = v.

Hypothesis get_after_set_different_key :
  forall m k1 k2 v, k1 <> k2 -> get k1 (set k2 v m) = get k1 m.

Definition abstract_transfer (key1 key2 : string) (amount : Z) (m : accounts) :=
  let key1_balance := get key1 m in
  let intermediate := set key1 (key1_balance - amount)  m in
  let key2_balance := get key2 intermediate in
  set key2 (key2_balance  + amount) intermediate.

Lemma get_after_abstract_transfer_at_emitter :
  forall (key1 key2 : string) (m : accounts) (amount : Z),
  (key1 <> key2 ->
   get key1 (abstract_transfer key1 key2 amount m) = get key1 m - amount).
Proof.
intros key1 key2 m1 amount.
intros key1_is_not_key2.
unfold abstract_transfer.
rewrite get_after_set_different_key.
rewrite get_after_set_same_key.
all: easy.
Qed.

Lemma get_after_abstract_transfer_when_emitter_is_receiver :
  forall (key1 key2 : string) (m1 m2: accounts) (amount : Z),
  m2 = abstract_transfer key1 key2 amount m1 ->
  (key1 = key2 -> get key1 m2 = get key1 m1).
Proof.
intros key1 key2 m1 m2 amount m2_value.
intros key1_is_key2.
rewrite m2_value.
unfold abstract_transfer.
rewrite <- key1_is_key2.
rewrite get_after_set_same_key.
rewrite get_after_set_same_key.
ring.
Qed.

Lemma get_after_abstract_transfer_at_receiver :
  forall (key1 key2 : string) (m1 m2: accounts) (amount : Z),
  m2 = abstract_transfer key1 key2 amount m1 ->
  key1 <> key2 ->
  get key2 m2 = get key2 m1 + amount.
Proof.
intros key1 key2 m1 m2 amount m2_value key1_is_not_key2.
rewrite m2_value.
unfold abstract_transfer.
rewrite get_after_set_same_key.
rewrite get_after_set_different_key.
easy.
now auto.
Qed.

Lemma get_after_abstract_transfer_at_different_key :
  forall (key1 key2 : string) (m1 m2: accounts) (amount : Z),
  m2 = abstract_transfer key1 key2 amount m1 ->
  forall key, key <> key1 /\ key <> key2 ->
  get key m2 = get key m1.
Proof.
intros key1 key2 m1 m2 amount m2_value key [key_is_not_key1 key_is_not_key2].
rewrite m2_value.
unfold abstract_transfer.
rewrite !get_after_set_different_key; easy.
Qed.

Definition weak_transfer (key1 key2 : string) (amount : Z) (m : accounts) :=
  let v1 := get key1 m in let v2 := get key2 m in
   set key2 (v2 + amount) (set key1 (v1 - amount) m).

Lemma get_after_weak_transfer_at_emitter :
  forall key1 key2 amount m,
  get key1 (weak_transfer key1 key2 amount m) = get key1 m - amount.
Proof.
intros key1 key2 amount m.
unfold weak_transfer.
rewrite get_after_set_different_key.
rewrite get_after_set_same_key.
easy.
Abort.

Lemma get_after_weak_transfer_same_key :
  forall key amount m,
  get key (weak_transfer key key amount m) = get key m + amount.
Proof.
intros key amount m.
unfold weak_transfer.
rewrite get_after_set_same_key.
easy.
Qed.

End abstract_program.

Section list_map_with_keys_of_type_string.

Variable A : Type.

Variable default_value : A.

Definition list_map := list (string * A).

Definition empty : list_map := nil.

Fixpoint  get (key : string) (m : list_map) :=
  match m with
  | nil => default_value
  | (s, v) :: m' => if String.eqb key s then v else get key m'
  end.

Fixpoint set (key : string) (v : A) (m : list_map) :=
  match m with
  | nil => (key, v) :: nil
  | (s, v') :: m' => if String.eqb key s then (s, v) :: m' else (s, v') :: set key v m'
  end.

Lemma get_after_set_same_key (key : string) (v : A) (m : list_map) :
  get key (set key v m) = v.
Proof.
induction m as [ | [s v'] m' Ih].
  simpl.
  rewrite String.eqb_refl.
  easy.
simpl.
destruct (key =? s)%string eqn:test_keys.
  simpl.
  rewrite test_keys.
  easy.
simpl.
rewrite test_keys.
exact Ih.
Qed.

Lemma get_after_set_different_key (key1 key2 : string) (v : A) (m : list_map) :
  key1 <> key2 -> get key1 (set key2 v m) = get key1 m.
Proof.
rewrite <- eqb_neq.
intros test_keys.
induction m as [ | [s v'] m' Ih].
  simpl.
  rewrite test_keys.
  easy.
simpl.
destruct (key1 =? s)%string eqn:test_key1s.
  destruct (key2 =? s)%string eqn:test_key2s.
    simpl.
    (* absurd case, eky1 key2 differnt, but both equal s *)
    revert test_keys.
    rewrite String.eqb_eq in test_key2s.
    rewrite test_key2s, test_key1s.
    easy.
  simpl.
  rewrite test_key1s.
  easy.
destruct (key2 =? s)%string eqn:test_key2s.
  simpl.
  rewrite test_key1s.
  easy.
simpl.
rewrite test_key1s.
exact Ih.
Qed.

End list_map_with_keys_of_type_string.

Definition accounts := list_map Z.

Definition transfer : string -> string -> Z -> accounts -> accounts :=
   abstract_transfer (list_map Z) (set Z) (get Z 0).

Lemma get_after_transfer_when_emitter_is_receiver key1 key2 m1 m2 amount :
  m2 = transfer key1 key2 amount m1 ->
  key1 = key2 -> get Z 0%Z key1 m2 = get Z 0%Z key1 m1.
Proof.
apply get_after_abstract_transfer_when_emitter_is_receiver.
now intros m k v; apply get_after_set_same_key.
Qed.

Lemma get_after_transfer_at_emitter key1 key2 m1 m2 amount :
  m2 = transfer key1 key2 amount m1 ->
  key1 <> key2 -> get Z 0%Z key1 m2 = get Z 0%Z key1 m1 - amount.
Proof.
intros m2_value; rewrite m2_value.
apply get_after_abstract_transfer_at_emitter.
  now intros m k v; apply get_after_set_same_key.
now intros m k1 k2 v; apply get_after_set_different_key.
Qed.

Lemma get_after_transfer_at_receiver key1 key2 m1 m2 amount :
  m2 = transfer key1 key2 amount m1 ->
  key1 <> key2 -> get Z 0%Z key2 m2 = get Z 0%Z key2 m1 + amount.
Proof.
apply (get_after_abstract_transfer_at_receiver _ (set Z)).
  now intros m k v; apply get_after_set_same_key.
now intros m k1 k2 v; apply get_after_set_different_key.
Qed.

Lemma get_after_transfer_at_different_key:
  forall (key1 key2 : string) (m1 m2 : accounts) (amount : Z),
  m2 = transfer key1 key2 amount m1 ->
  forall key : string, key <> key1 /\ key <> key2 ->
   get Z 0 key m2 = get Z 0 key m1.
Proof.
intros key1 key2 m1 m2 amount.
apply get_after_abstract_transfer_at_different_key.
intros m k1 k2 v.
now apply get_after_set_different_key.
Qed.

Definition initial_state :=
  set Z "Julie" 3 (set Z "Claude" 2 (empty Z)).

Definition state_after_first_transaction :=  
  transfer "Julie" "Antoine" 2 initial_state.

Compute get Z 0 "Julie" state_after_first_transaction.

Module F := FMapAVL.IntMake Z_as_Int String_as_OT.

Definition avl_accounts := F.t Z.
Definition avl_set : string -> Z -> avl_accounts -> avl_accounts := @F.add Z.
Definition avl_empty := F.empty Z.

Arguments avl_set _%string _%Z _.
Definition avl_get (s : string) (m : avl_accounts) :=
  match F.find s m with
  | None => 0%Z
  | Some x => x
  end.

Lemma avl_get_after_set_same_key :
  forall (key : string) (v : Z) (m : avl_accounts),
  avl_get key (avl_set key v m) = v.
Proof.
intros key v m.
unfold avl_get, avl_set.
assert (add1 := F.add_1 m v (refl_equal key)).
now rewrite (F.find_1 add1).
Qed.

Lemma avl_get_after_set_different_key :
  forall (key1 key2 : string) (v : Z) (m : avl_accounts),
  key1 <> key2 -> avl_get key1 (avl_set key2 v m) = avl_get key1 m.
Proof.
intros key1 key2 v m key_is_not_key2.
unfold avl_get.
destruct (F.find key1 m) as [v1 | ] eqn:tstm.
  apply F.find_2 in tstm.
  apply (F.add_2 v (not_eq_sym key_is_not_key2)) in tstm.
  apply F.find_1 in tstm.
  now unfold avl_set; rewrite tstm.
assert (nomap : forall x, ~F.MapsTo key1 x m).
  intros x abs; apply F.find_1 in abs; congruence.
assert (nomap2 : forall x, ~F.MapsTo key1 x (avl_set key2 v m)).
  now intros x abs; apply F.add_3 in abs; auto;apply nomap in abs.
destruct (F.find key1 (avl_set key2 v m)) as [v1 | ] eqn:tst1; auto.
now apply F.find_2 in tst1; apply nomap2 in tst1.
Qed.

Definition avl_initial_state :=
  avl_set "Julie" 3 (avl_set "Claude" 2 avl_empty).

Compute avl_get "Julie" avl_initial_state.

Definition avl_transfer : string -> string -> Z ->
                 avl_accounts -> avl_accounts :=
  abstract_transfer avl_accounts avl_set avl_get.

Lemma get_after_avl_transfer_when_emitter_is_receiver key1 key2 m1 m2 amount :
  m2 = avl_transfer key1 key2 amount m1 ->
  key1 = key2 -> avl_get key1 m2 = avl_get key1 m1.
Proof.
apply get_after_abstract_transfer_when_emitter_is_receiver.
now intros m k v; apply avl_get_after_set_same_key.
Qed.

Lemma get_after_avl_transfer_at_emitter key1 key2 m1 m2 amount :
  m2 = avl_transfer key1 key2 amount m1 ->
  key1 <> key2 -> avl_get key1 m2 = avl_get key1 m1 - amount.
Proof.
intros m2_value; rewrite m2_value.
apply get_after_abstract_transfer_at_emitter.
  now intros m k v; apply avl_get_after_set_same_key.
now intros m k1 k2 v; apply avl_get_after_set_different_key.
Qed.

Lemma get_after_avl_transfer_at_receiver key1 key2 m1 m2 amount :
  m2 = avl_transfer key1 key2 amount m1 ->
  key1 <> key2 -> avl_get key2 m2 = avl_get key2 m1 + amount.
Proof.
apply (get_after_abstract_transfer_at_receiver _ avl_set).
  now intros m k v; apply avl_get_after_set_same_key; auto.
now intros m k1 k2 v; apply avl_get_after_set_different_key.
Qed.

Lemma get_after_avl_transfer_at_different_key:
  forall (key1 key2 : string) (m1 m2 : avl_accounts) (amount : Z),
  m2 = avl_transfer key1 key2 amount m1 ->
  forall key : string, key <> key1 /\ key <> key2 ->
   avl_get key m2 = avl_get key m1.
Proof.
intros key1 key2 m1 m2 amount.
apply get_after_abstract_transfer_at_different_key.
intros m k1 k2 v.
now apply avl_get_after_set_different_key.
Qed.

Definition after_transaction :=
  avl_transfer "Julie" "Claude" 2 avl_initial_state.

Compute avl_get "Julie" after_transaction.

Definition after_transaction2 :=
  avl_transfer "Julie" "Julie" 2 after_transaction.

Compute avl_get "Julie" after_transaction2.

Compute (F.elements avl_initial_state, F.elements after_transaction, F.elements after_transaction2).
