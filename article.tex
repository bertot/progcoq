\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\title{Prouvez que vos programmes fonctionnels n'ont pas de bugs avec Coq}

\author{Yves Bertot}
\begin{document}
\maketitle
\input intro_prog_fun.tex
\input abstract_transfer.tex
\subsection*{Détecter des bugs dans des programmes}
La fonction {\tt abstract\_transfer} n'est pas parfaite, mais elle a
un comportement acceptable même lorsque les comptes émetteurs et
destinataires du transfert sont les mêmes.  Il est intéressant de voir
ce qui se passe pour un code qui ne prend pas de précautions.

Voici une autre fonction pour répondre au même besoin, mais avec un
comportement erroné si les deux clefs sont égales.
\begin{verbatim}
Definition weak_transfer (key1 key2 : string) (amount : Z) (m : accounts) :=
  let v1 := get key1 m in let v2 := get key2 m in
   update key2 (v2 + amount) (update key1 (v1 - amount) m).
\end{verbatim}
Un tel choix peut être délibéré, mais il peut aussi être le résultat
d'une maladresse.
Si notre programmeur maladroit oublie le cas où les deux clefs peuvent
être êgales au moment d'écrire le programme, il peut aussi oublier
d'inclure cette condition dans l'énoncé de la propriété.  Il se
lancera donc probablement dans la preuve impossible suivante:
\begin{verbatim}
Lemma get_after_weak_transfer_at_emitter :
  forall key1 key2 amount m,
  get key1 (weak_transfer key1 key2 amount m) = get key1 m - amount.
Proof.
intros key1 key2 amount m.
unfold weak_transfer.
rewrite get_after_update_different_key.
rewrite get_after_update_same_key.
easy.
\end{verbatim}
La réponse du système à la fin de cet ébauche de preuve montre un but
improvable: nous ne savons rien sur {\tt key1} et {\tt key2} à part
leur type et nous n'avons donc pas les moyens de prouver qu'elles
sont différentes.
\begin{verbatim}
 key1, key2 : string
  amount : Z
  m : accounts
  ============================
  key1 <> key2
\end{verbatim}
Au moment d'utiliser l'hypothèse {\tt
  get\_after\_update\_different\_key} un but supplémentaire est bien
créé qui demande de prouver que {\tt key1} et {\tt key2} sont bien des
clefs différentes.  Cette obligation n'est pas résolue par les
commandes suivantes et sa présence permet de rendre explicite pour le
programmeur le fait qu'il avait fait une hypothèse implicite.

Cet échec de preuve doit inciter le programmeur à étudier le
comportement de la fonction plus précisément.  Premièrement, l'énoncé
du théorème de {\tt get\_after\_weak\_transfer\_at\_emitter} doit être
corrigé pour inclure la condition manquante (comme dans le théorème
que nous avons montré précédemment pour {\tt abstract\_transfer}.
Deuxièmement, on peut envisager un énoncé supplémentaire pour couvrir
le cas où les deux clefs sont les mêmes.  L'ébauche de preuve pour ce
cas permet alors de mieux comprendre le comportement erroné.  Le
calcul symbolique permet de se rendre à l'évidence: la valeur
retournée dans ce cas n'est pas satisfaisante.  Comme le montre le
théorème suivant, que nous arrivons à prouver.
\begin{verbatim}
Lemma get_after_weak_transfer_same_key :
  forall key amount m,
  get key (weak_transfer key key amount m) = get key m + amount.
Proof.
intros key amount m.
unfold weak_transfer.
rewrite get_after_update_same_key.
easy.
Qed.
\end{verbatim}
Notre programmeur a maintenant plusieurs possibilités.  Soit il ne corrige
pas le code, mais les propriétés qu'il fournit (et qu'il arrive à
prouver) documentent explicitement le fait que la fonction {\tt
  weak\_transfer} ne doit pas être utilisée lors que l'émetteur et
le destinataire du transfert sont donnés par la même clef.  Le risque
de bug est diminué grâce à cette documentation.  Une deuxième
possibilité est de corriger le code pour arriver au code de {\tt
  abstract\_transfer} qui apparait plus haut.  Une troisième
possibilité est d'inclure dans le code un test pour
vérifier que les clefs sont différentes et ne rien faire lorsque cette
condition n'est pas satisfaite.

\subsection*{La fonction {\tt abstract\_transfer} est-elle parfaite?}
Le nom d'une fonction peut parfois être trompeur.  Dans la vie de tous
les jours, lorsque l'on envisage un virement d'un compte vers un
autre, on pense naturellement que la somme transférée est positive.
La fonction {\tt abstract\_transfer} ne vérifie pas cette propriété.
Un utilisateur malhonnête qui aurait accès à cette fonction sans
contrôle supplémentaire pourrait effectuer un transfert inversé prenant
l'argent d'un autre compte pour approvisionner son propre compte.

Une application financière qui donnerait à chaque utilisateur accès à
une fonction de transfert devrait donc ajouter des preuves de
correction supplémentaires.  D'une part, il faudrait écrire une
propriété montrant que la somme globale gérée par la base de données
ne change pas dans les opérations de transfert.  D'autre part, il
faudrait écrire une propriété montrant qu'un transfert émis par un
utilisateur peut seulement faire diminuer la somme associée à cet
utilisateur.

\subsection*{Développer un exemple de base de données}
Nous allons maintenant expliquer comment fournir un exemple
simple et peu efficace de base de données, en exploitant la possibilité
en Coq de programmer avec des listes.  Une base de donnée sera
simplement une liste de couples associant une chaine de caractères et
une valeur associée.  Nous montrerons comment
développer une base de données où la valeur associée n'est pas
nécessairement de type entier.  Pour cet exemple, nous utilisons
encore une section.
\begin{verbatim}
Section list_map_with_keys_of_type_string.

Variable A : Type.

Variable default_value : A.

Definition list_map := list (string * A).

Definition empty : list_map := nil.

Fixpoint  get (key : string) (m : list_map) :=
  match m with
  | nil => default_value
  | (s, v) :: m' => if String.eqb key s then v else get key m'
  end.

Fixpoint update (key : string) (v : A) (m : list_map) :=
  match m with
  | nil => (key, v) :: nil
  | (s, v') :: m' => if String.eqb key s then (s, v) :: m' else (s, v') :: update key v m'
  end.
\end{verbatim}
En Coq, les listes sont notées {\tt nil} pour représenter une liste
vide, ou {\tt \(a\) :: \(l\)} pour représenter une liste dont le
premier élément est \(a\) et les autres éléments sont données par
\(l\).  Pour traiter tous les éléments d'une liste, Coq fournit des
fonctions {\em récursives}, que l'on définit avec le mot-clef {\tt
  Fixpoint}.  Ici, nous avons défini deux fonctions récursives.  Pour
la fonction {\tt get} on exprime que pour trouver la valeur associée à
une clef particulière, on doit faire face à deux cas: si la liste est
vide, alors on retourne une valeur par défault (ici représentée par
{\tt default\_value}, dont l'environnement de travail suppose l'existence).
Le deuxième cas est lors que la liste n'est pas vide.  Elle alors un
premier élément qui est le couple d'une chaine caractère (nommé {\tt
  s} dans le code de {\tt s}) et d'une valeur {\tt v}.  Si la chaine
{\tt s} est la même que la clef {\tt key} pour laquelle on veut
obtenir une valeur, alors c'est {\tt v} qui est retournée.  Si {\tt s}
et {\tt key} sont différentes, alors c'est dans le reste de la liste,
ici appelée {\tt m'} qu'il faut chercher.  Ici, nous profitons du fait
que la fonction {\tt get} en cours de définition est récursive et nous
utilisons cette fonction elle-même pour expliquer comment faire la
recherche.

La définition de la fonction {\tt get} repose sur une construction
{\tt match} \dots {\tt with} \dots {\tt end}, que l'on appelle
construction de filtrage qui est spécifique de la programmation
fonctionnelle typée.  Cette construction exploite le fait que le type
des listes repose sur deux cas possibles.  La théorie de Coq garantit
que les fonction récursives comme {\tt get} n'ont pas de bugs de type
pointeur null, et que ces fonctions retournent toujours un résultat du
type attendu.  La fonction {\tt update} est similaire.

Lorsque l'on veut faire des preuves sur les programmes récursifs, on
est amené à utilisér trois nouvelles commandes de preuve.  La plus
remarquable est la tactique {\tt induction} qui est typiquement pour
faire des preuves par récurrence et est pratiquement toujours
nécessaire pour raisonner sur les fonctions récursives.  La deuxième
est la commande {\tt simpl}, qui remplace la commande {\tt unfold} que
nous avons déjà vue pour travailler spécifiquement sur les fonctions
récursives.  La troisième commande est {\tt destruct} qui est adaptée
pour raisonner sur les constructions de filtrage.

Voici deux exemples de preuves, où nous établissons pour cette définition
de {\tt get} et {\tt update} les propriétés attendues dans les hypothèses
de la section {\tt abstract\_program}.
\begin{verbatim}
Lemma get_after_update_same_key (key : string) (v : A) (m : list_map) :
  get key (update key v m) = v.
Proof.
induction m as [ | [s v'] m' Ih].
  simpl.
  rewrite String.eqb_refl.
  easy.
simpl.
destruct (key =? s)%string eqn:test_keys.
  simpl.
  rewrite test_keys.
  easy.
simpl.
rewrite test_keys.
exact Ih.
Qed.

Lemma get_after_update_different_key (key1 key2 : string) (v : A) (m : list_map) :
  key1 <> key2 -> get key1 (update key2 v m) = get key1 m.
Proof.
rewrite <- eqb_neq.
intros test_keys.
induction m as [ | [s v'] m' Ih].
  simpl.
  rewrite test_keys.
  easy.
simpl.
destruct (key1 =? s)%string eqn:test_key1s.
  destruct (key2 =? s)%string eqn:test_key2s.
    simpl.
    (* absurd case, eky1 key2 differnt, but both equal s *)
    revert test_keys.
    rewrite String.eqb_eq in test_key2s.
    rewrite test_key2s, test_key1s.
    easy.
  simpl.
  rewrite test_key1s.
  easy.
destruct (key2 =? s)%string eqn:test_key2s.
  simpl.
  rewrite test_key1s.
  easy.
simpl.
rewrite test_key1s.
exact Ih.
Qed.

End list_map_with_keys_of_type_string.
\end{verbatim}
Parce que nous n'avons pas affaire à un type d'entier, nous avons
quelques obligation différentes de ce qui était décrit dans la section
{\tt abstract\_program}.  En particuler les fonction {\tt update} et
{\tt get} prennent un premier argument initial qui décrit
explicitement le type des valeurs associées aux clefs dans la base de
données.  En outre, la fonction d'exploration
{\tt get} doit pouvoir retourner une valeur même quand aucune valeur
n'a été associée à la clef dans la base de données.  Pour cette
raison, la fonction {\tt get} prend un deuxième argument
supplémentaire qui est la valeur par défaut retournée dans le cas où
une valeur n'est pas déjà associée pour la clef choisie.
\end{document}
