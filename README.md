# Programmez avec Coq

Une description de la programmation en Coq pour un magazine  grand public.

## Structure de ce dépot

Ce dépôt contient un article en français pour illustrer l'utilisation de Coq
dans un case de programmation simple: la programmation d'opérations de
transfert entre des comptes bancaires.  Le point de vue est que l'on ne
connait pas à l'avance la structure de données utilisée pour gérér les
différents comptes, on suppose seulement que l'on dispose de deux fonctions,
la première pour interroger une base de données (les clefs utilisées
étant des chaines de caractères) et la seconde étant utilisée pour mettre
à jour le montant pour une clef donnée.

Dans ce cadre, on montre comment programmer une opération de transfer et
prouver que cette opération de transfer a un comportement attendu.  Ces preuves
font apparaitre qu'il faut faire attention au cas où le compte émetteur et
le compte destinataire du transfert sont le même compte.  Ces preuves
permettent d'illustrer des étapes simples de preuve (essentiellement des
preuves par réécriture conditionnelle).

Dans une deuxième étape, on montre qu'une version plus naive de l'opération
de transfert peut avoir un comportement érroné dans certains cas et on illustre
comment la preuve permet de découvrir l'erreur et de trouver un remède.

Dans une troisième étape, on montre comment définir une structure de données
pour représenter l'ensemble des comptes gérés par le logiciel.  L'exemple
choisi repose sur les listes, et le style de programmation repose sur de
la récursion.  En conséquence, les preuves de correction font à leur tour
appel à des techniques de preuve par récurrence.

Dans une quatrième étape, on montre comment la structure de données basée
sur les listes peut être remplacée par une structure plus efficace, déjà
fournie par les bibliothèques de Coq.

Le contenu de l'article est décomposé en plusieurs parties, de telle manière
que l'on peut composer une version longue `article.tex`
(pas encore bien finie à l'heure
où j'écris ces lignes) ou une version courte `short_article.tex`.

## Fabrication de l'article au format `rtf`

Cet article et ses dérivés sont destinez au magazine "Programmez!" et le
rédacteur de ce magazine demande qu'on lui fournisse des documents au format
`rtf`.  Pour produire ces documents, j'utilise la commande `latex2rtf` comme
 suit:

```
latex2rtf -i french -o short_article.rtf short_article.tex
latex2rtf -i french -o article.rtf article.tex
```
## Visualisation des différences avec `latexdiff`

Le fichier principal contient des inclusions de fichiers annexes par la macro
`\input`.  Pour visualiser les différences, il faut d'abord tout remettre
dans un fichier unique.  La script `perl` `make_single_short.pl` fait cette
opération.  Pour obtenir la différence entre la branch `main` et une vieille
révision, il faut effectuer les opérations suivantes:

```
git checkout main
cp make_single_short.pl tmp.pl
./make_single_short.pl > expanded_short.tex
git checkout e92dfdf9 # choose you own revision identifier here
./tmp.pl > old_expanded_short.tex
latexdiff old_expanded_short.tex expanded_short.tex > pre_diff.tex
sed -e "/DIF/s/.url//" pre_diff.tex > diff.tex
pdflatex diff
git checkout main
```
