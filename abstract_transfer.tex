\subsection*{Prouver des propriétés de programmes fonctionnels purs avec Coq}

La raison d'être de Coq est de prouver que certains programmes
satisfont des propriétés.  Ces propriétés peuvent alors servir
d'informations pour vérifier que plusieurs bibliothèques logicielles
sont utilisées de façon cohérente.

Le premier niveau de cohérence est fourni par le typage: toutes les valeurs
manipulées dans les programmes Coq ont un type et toutes les fonctions
prenant des arguments indiquent quel est le type attendu.  Vérifier
que chaque fonction reçoit en entrée des valeurs du bon type est l'une
des premières étapes pour éviter les erreurs de programmation.

Les différents langages fonctionnels typés se distinguent par le
pouvoir expressif des types présents dans ce
langage.  Souvent ces types donnent des informations basiques: être un
nombre entier, une chaine de caractères, ou bien une valeur de
vérité.  Dans Coq, le type peut contenir des informations logiques ou
mathématiques beaucoup plus avancées.  Par exemple, on peut y définir
le type des nombres compris entre 0 et 100, le type des
nombres premiers, ou le type des listes triées de nombres.

Comme illustration, nous allons imaginer un cas d'usage où l'on développe
une application financière associant à une collection
d'invidus (identifiés par une chaine de caractères) des sommes
d'argent.  Dans un premier temps, nous supposerons l'existence d'une
bibliothèque fournissant une base de données avec seulement deux
opérations: consulter la base de données pour un identifiant et
fabriquer une nouvelle base de données à partir d'une base de données
existante, où seule la valeur associée à un identifiant est modifiée.

\subsection*{Démarrer une session Coq}
Le système Coq est disponible à l'adresse suivante: {\url https://coq.inria.fr},
il est possible de télécharger des programmes binaires pour installer sur
votre ordinateur, mais il est également possible de faire tourner une
instance de ce programme dans votre navigateur web, grâce aux capacités des
navigateurs moderne à faire tourner des logiciels écrits en javascript ou
web assembly.

Avant de démarrer notre expérience, nous devons demander à Coq de charger
quelques bibliothèques qui fourniront des structures de données basiques.

\begin{verbatim}
Require Import String List ZArith Bool.

Open Scope Z_scope.
\end{verbatim}
La première ligne est utilisée pour indiquer que nous allons
utiliser des bibliothèques existantes de coq pour manipuler des
chaines de caractères, des listes, des valeurs entières, et des
valeurs booléennes.

La deuxième ligne indique que les opération arithmétiques feront
référence à des opérations entre nombres entiers par défaut.

\subsection*{Comment transférer une somme d'argent d'un compte vers un autre?}

Pour transférer de l'argent d'un compte vers un autre, nous voulons effectuer
les opérations suivantes:

\begin{enumerate}
\item Trouver la somme d'argent courante du compte émetteur.

\item Retirer de cette somme d'argent la somme transférée.

\item Trouver la somme d'argent courante du compte receveur.

\item Ajouter à cette somme la somme transférée.

\item Modifier les deux comptes pour qu'ils enregistrent les nouvelles sommes.
\end{enumerate}
Le diable est dans les détails, suivant l'ordre dans lequel ces
opérations sont effectuées il est possible que le programme
représente un risque de perte d'argent pour l'émetteur ou pour
l'organisme gestionnaire des comptes.

\subsection*{Travailler avec des programmes inconnus}
L'une des particularités de Coq est de permettre d'écrire des
programmes et de raisonner sur ces programmes même si
certaines parties ne sont pas encore définies.

Dans notre exemple, nous voulons commencer à écrire notre programme de
transfert, même si nous ne disposons pas des programmes pour manipuler
la base de données.  Nous le faisons de la façon suivante:
\begin{verbatim}
Section abstract_program.

Variable accounts : Type.

Variable empty : accounts.

Variable update : string -> Z -> accounts -> accounts.

Variable get : string -> accounts -> Z.
\end{verbatim}
La première ligne indique que nous entrons dans un espace où nous
allons {\em supposer l'existence} de nouveaux objets.

Dans la deuxième ligne, nous supposons
qu'il existe un type {\tt accounts} pour les bases de données associant
des clés qui sont des chaines de caractères et des valeurs qui sont des
entiers.  Puis nous supposons qu'il existe
une base de données vide
{\tt empty}, et qu'il existe deux fonctions, la fonction {\tt update}
pour construire une nouvelle base de données à partir d'une base
existante en modifiant seulement la valeur pour une clef, et la
fonction {\tt get} pour connaitre la données associée à une clef dans
une base de données.

Lorsque l'on suppose l'existence d'une fonction, on doit donner son type.
Ici, nous indiquons que la fonction {\tt update} a le type suivant:
\begin{center}
\tt  string -> Z -> accounts -> accounts
\end{center}
Ceci exprime que cette fonction prend trois arguments, le premier doit
être de type {\tt string} (un type pour représenter les chaines de
caractères),
le second argument argument doit être de type {\tt Z} (un type pour
représenter les entiers relatifs non bornés), et le troisième doit
être de type {\tt accounts}.  Le deuxième mot {\tt accounts} qui apparait
sur cette ligne indique le type de la valeur retournée.
Prise dans son ensemble, cette ligne exprime que la valeur
retournée par la fonction {\tt update} appliquée à trois arguments est
de type {\tt accounts}.

Avec ces premières lignes, rien n'indique quel doit être le
comportement des fonctions {\tt update} et {\tt get}.  En fait, nous
n'avons pas besoin de savoir grand chose sur ces fonctions.  En
premier, nous avons besoin de savoir qu'interroger une base de données
obtenue par la fonction {\tt update \(k\) \(v\) \(m\)} pour la même
clef \(k\) retourne la valeur \(v\).  Nous exprimons ceci par
l'hypothèse suivante, qui s'écrit également dans le langage de Coq:
\begin{verbatim}
Hypothesis get_after_update_same_key :
  forall m k v, get k (update k v m) = v.
\end{verbatim}
Ces deux lignes montrent que l'on utilise le texte
``{\tt update k v m}'' pour
représenter l'appel de la fonction {\tt update} avec les trois
arguments {\tt k}, {\tt v}, {\tt m}.  Le texte
\begin{verbatim}
  get k (update k v m)
\end{verbatim}
est donc l'application de la fonction {\tt get} à deux arguments, le
deuxième étant le résultat de l'appel de la fonction {\tt update} aux
trois arguments {\tt k}, {\tt v}, et {\tt m}.

Nous avons aussi besoin d'exprimer que les autres valeurs ne sont pas
modifiées par la fonction {\tt update}.  En d'autres termes, si nous
interrogeons une base de données obtenue par la fonction {\tt update
  \(k\) \(v\) \(m\)} pour une clef différente, nous obtenons la même
valeur que pour la base de données \(m\).  Nous exprimons ceci par
l'hypothèse suivante:
\begin{verbatim}
Hypothesis get_after_update_different_key :
  forall m k1 k2 v, k1 <> k2 -> get k1 (update k2 v m) = get k1 m.
\end{verbatim}
Dans les énoncés de ces deux hypothèses, nous utilisons le mot-clef
{\tt forall} pour exprimer que la propriété logique est satisfaite
pour tous les choix possibles de {\tt m} {\tt k}, ou {\tt v} pour le
premier énoncé, et pour
tous les choix possible de {\tt m}, {\tt k1}, {\tt k2}, et {\tt v}
pour le second énoncé.  La notation {\tt k1 <> k2} est utilisée pour
représenter l'énoncé logique ``{\tt k1} est différent de {\tt k2}'' et
la flèche est utilisée pour représenter l'implication.

Dans ce contexte abstrait où nous ne savons pas comment les fonctions
{\tt update} et {\tt get} sont codées, nous pouvons quand même décrire
l'algorithme de transfert.  Les deux hypothèses nous servent à
documenter le comportement attendu pour ces deux fonctions.

Nous pouvons décrire l'opération de transfert de la façon suivante:
\begin{verbatim}
Definition abstract_transfer 
  (key1 key2 : string) (amount : Z) (m : accounts) :=
  let key1_balance := get key1 m in
  let intermediate := update key1 (key1_balance - amount) m in
  let key2_balance := get key2 intermediate in
  update key2 (key2_balance  + amount) intermediate.
\end{verbatim}
La première lignes indique que nous définissons une fonction appelée
{\tt abstract\_transfer}, la deuxième ligne
indique que cette fonction reçoit quatre arguments: les deux
premiers sont des clefs que nous appellerons {\tt key1} et {\tt key2},
de type {\tt string}, le troisième argument est un entier et le
quatrième argument est une base de données {\tt m}.  Dans cette
définition, nous n'indiquons pas quel est le type de retour, mais
cette information pourra être déduite de la forme du code: le type
de retour est le même que celui de la fonction {\tt update} lorsque
cette dernière est appliquée à trois arguments, {\tt accounts}.

Les différentes étapes que nous avons décrites plus tôt dans un texte en
français sont rendues explicites dans le code: en ligne 3 on récupère
la valeur associée à la première clef dans la base de données, en ligne
4 on fait la soustraction du montant transféré et on fabrique une
nouvelle base de données associant cette nouvelle valeur à la première
clef, en ligne 5, on récupère la valeur associée à la deuxième clef
dans la base de données et en ligne 6 on fait l'addition du montant
transféré et la mise à jour pour produire la base de données finale.

\subsection*{Tester notre programme pour un cas simple}
Parce que nous disposons d'hypothèses sur les fonctions {\tt get} et
{\tt update}, nous pouvons Faire des tests symboliques de notre programme.

Par exemple, nous pouvons imaginer un cas d'usage où Alice a 10 pièces de
monnaies dans son compte, ceci est décrit par
un état initial que nous appelons {\tt accounts1}.  Dans une première
transaction, Alice donne 2 pièces à Bernard, et nous obtenons un nouvel
état que nous appelons {\tt accounts2}, plus tard Bernard donne 3 pièces de
monnaies à {\tt Alice}, et ceci donne encore un nouvel état que nous appelons
{\tt accounts3}.  Ces différents états peuvent être décrits de la manière
suivante:
\begin{verbatim}
Variable accounts1 : accounts.
Hypothesis balance_1_Alice : get "Alice" accounts1 = 10.

Let accounts2 := abstract_transfer "Alice" "Bernard" 2 accounts1.
Let accounts3 := abstract_transfer "Bernard" "Alice" 3 accounts2.
\end{verbatim}
Maintenant, nous pouvons utiliser Coq pour vérifier que le solde pour
Alice dans le dernier état est de 11.
\begin{verbatim}
Lemma balance_3_Alice : get "Alice" accounts3 = 11.
Proof.
unfold accounts3, accounts2, abstract_transfer.
rewrite get_after_update_same_key.
rewrite get_after_update_different_key.
rewrite get_after_update_different_key.
rewrite get_after_update_same_key.
rewrite balance_1_Alice.
all:easy.
Qed.
\end{verbatim}
La première ligne décrit le test que nous voudrions vérifier.  La deuxième
ligne contient un mot-clef indiquant que nous voyons ce test comme une preuve
que nous allons faire pas-à-pas.  La troisième ligne indique à Coq d'expanser
la définition de {\tt accounts3}, {\tt accounts2}, et {\tt abstract\_transfer}.
Les autres fonctions et valeurs ne peuvent pas être expansées, parce qu'elles
sont en fait inconnues, mais nous pouvons utiliser les hypothèses faites
jusqu'à maintenant pour continuer notre test.

Lorsque nous envoyons ces commandes au système Coq, il nous répond en donnant
un but, dans lequel l'état en cours du calcul apparait.  Ici, nous voyons une
égalité dont le membre droit est 11 et dont le membre gauche est une
expression composée d'appels à {\tt get} et {\tt update}.  Nous pouvons
réduire cette expression avec les 5 lignes suivantes.  A la fin, il ne reste
que trois faits à vérifier.  Le premier fait est une égalité entre nombres
entiers, les deux autres faits sont le même fait dupliqué : il s'agit
de vérifier que
les deux chaines de caractères {\tt "Alice"} et {\tt "Bernard"} sont distinctes.
Ces faits sont vérifiés par la commande {\tt easy}.
Après la commande {\tt easy} tout a été vérifié et le test est concluant, il
faut envoyer la commande {\tt Qed} pour indiquer que la preuve est terminée,
sinon le système ne nous laissera pas commencer une nouvelle preuve.

Nous n'avons pas besoin de savoir comment les fonctions
{\tt get} et {\tt update} sont codées pour effectuer ce test.  Le code 
de {\tt abstract\_transfer} est donc symboliquement exécutable avant d'avoir
développé la librairie sur laquelle il repose.  De même, nous
n'avons pas besoin de savoir combien d'argent est disponible sur le compte de
Bernard.
\subsection*{Prouver des propriétés de notre programme}
Pour être sûr que cette fonction fait ce que nous attendons, il est
important de prouver quelques propriétés, qui pourront également
servir de documentation pour les utiliseurs de ce code.  La première propriété
importante est que les valeurs associées à toutes les clefs
différentes des clefs concernées restent inchangées.

La deuxième propriété est que le montant transféré est bien retranché
du compte émetteur, du moins lorsque ce compte émetteur est différent
du compte destinataire.  La troisième propriété est que le montant
transféré est bien ajouté au compte destinataire, également lorsque le
compte émetteur est différent.  Enfin, une quatrième propriété doit
exprimer que le compte émetteur n'est pas modifié s'il est identique
au compte destinataire.

Observons maintenant comment la première propriété est vérifiée à
l'aide du système de preuve.  La première étape est d'énoncer la
propriété que nous voulons prouver:
\begin{verbatim}
 Lemma get_after_abstract_transfer_at_emitter :
  forall (key1 key2 : string) (m : accounts) (amount : Z),
  key1 <> key2 ->
   get key1 (abstract_transfer key1 key2 amount m) =
   get key1 m - amount.
Proof.
\end{verbatim}
La première ligne indique seulement que nous voulons commencer une
preuve et elle donne le nom donné à cette propriété pour un usage
ultérieur.  La deuxième ligne indique que nous voulons établir une
propriété logique pour toutes les valeurs possibles des clefs
{\tt key1} et {\tt key2}, toute base de données {\tt m} et tout
montant {\tt amount}.  La troisième ligne contient la partie gauche
d'une implication, donc le début d'une phrase ``si {\tt key1} est
différent de {\tt key2}''.  Les deux dernières lignes contiennent
le reste de
l'implication exprimant que la valeur associée à {\tt key1} après le
transfert est modifiée de l'opposé du montant par rapport à la valeur
dans {\tt m}.

Lorsque cette commande est envoyée au système de preuve, celui-ci
répond en affichant le texte suivant:
\begin{verbatim}
1 goal (ID 15)
  
  accounts : Type
  empty : accounts
  update : string -> Z -> accounts -> accounts
  get : string -> accounts -> Z
  get_after_update_same_key :
     forall (m : accounts) (k : string) (v : Z),
        get k (update k v m) = v
  get_after_update_different_key :
     forall (m : accounts) (k1 k2 : string) (v : Z),
        k1 <> k2 ->
        get k1 (update k2 v m) = get k1 m
  ============================
  forall (key1 key2 : string) (m : accounts) (amount : Z),
  key1 <> key2 ->
  get key1 (abstract_transfer key1 key2 amount m) =
  get key1 m - amount
\end{verbatim}
Cette réponse se décompose en deux parties, séparée par la barre
horizontale {\tt ============================}.

La partie supérieure contient les choses dont nous supposons
l'existence et les propriété supposées.  Cette partie supérieure est
appelée le {\em contexte} du but.  Ici, nous retrouvons les
différents objets introduits depuis l'ouverture de la section et les
deux hypothèses.  La partie inférieure contient une formule logique
que nous devons prouver.  Cette partie inférieure est appelée la {\em
  conclusion} du but.  A cette étape, c'est exactement l'énoncé que
nous avons donné.

Cette énoncé commence par une quantification universelle sur quatre
nouveaux objets.  Le mode de raisonnement usuel est simplement de
fixer quatre constantes pour ces quatres quantification universelle.
Si nous devons montrer qu'une formule est vraie pour tous les choix
possibles des quatres variables, nous pouvons choisir une valeur
arbitraire pour chacune de ces variables et vérifier que nous savons
montrer la formule pour ce choix arbitraire.  La commande pour
effectuer cette étape de raisonnement est la suivante:
\begin{verbatim}
intros key1 key2 m1 amount.
\end{verbatim}
La réponse de Coq est la suivante:
\begin{verbatim}
1 goal (ID 19)
  
  accounts : Type
  empty : accounts
  update : string -> Z -> accounts -> accounts
  get : string -> accounts -> Z
  get_after_update_same_key :
     forall (m : accounts) (k : string) (v : Z),
        get k (update k v m) = v
  get_after_update_different_key :
     forall (m : accounts) (k1 k2 : string) (v : Z),
        k1 <> k2 ->
        get k1 (update k2 v m) = get k1 m
  key1, key2 : string
  m1 : accounts
  amount : Z
  ============================
  key1 <> key2 ->
  get key1 (abstract_transfer key1 key2 amount m1) =
  get key1 m1 - amount
\end{verbatim}
Nous voyons que trois nouvelles lignes ont été ajoutées dans le
contexte, et la conclusion est devenue une formule plus simple qui ne
contient plus de quantification universelle.

La formule que devons prouver maintenant est une implication.  La
méthode usuelle pour prouver une implication est de montrer que le
membre droit de l'implication peut être prouvé si le membre gauche est
satisfait.  Ici pour exprimer que le membre gauche est satisfait, il
suffit de l'ajouter dans le contexte, ce que nous faisons avec la
commande suivante.
\begin{verbatim}
intros key1_is_not_key2.
\end{verbatim}
Le but a maintenant la forme suivante (les premières lignes sont
éludées):
\begin{verbatim}
  key1_is_not_key2 : key1 <> key2
  ============================
  get key1 (abstract_transfer key1 key2 amount m1) =
  get key1 m1 - amount
\end{verbatim}
Nous devons maintenant montrer l'égalité entre deux valeurs
numériques.  Celle de gauche mentionne la fonction {\tt
  abstract\_transfer}.  Nous allons demander au système de remplacer
cette occurrence de la fonction par sa définition.
\begin{verbatim}
unfold abstract_transfer.
\end{verbatim}
La réponse est:
\begin{verbatim}
  ============================
  get key1
    (update key2
       (get key2 
            (update key1
               (get key1 m1 - amount)
               m1)
            + amount)
       (update key1 (get key1 m1 - amount) m1)) =
  get key1 m1 - amount
\end{verbatim}
Le membre gauche de l'équation que nous voulons prouver est une
expression qui commence par l'interrogation (par la fonction {\tt
  get}) pour la clef {\tt key1} d'une base de données obtenue par la
mise à jour pour la clef {\tt key2}.  Le comportement dans ce cas de
figure est prévu par l'hypothèse {\tt
  get\_after\_update\_different\_key}.
\begin{verbatim}
rewrite get_after_update_different_key.
\end{verbatim}
Nous avons maintenant deux buts: dans le premier le comportement
prévu permet de réduire le membre gauche de l'équation.  Dans le
deuxième but, nous devons prouver la condition nécessaire pour que
cette hypothèse soit utilisée: les deux clefs doivent effectivement
être différentes.  Ce deuxième but est conservé pour plus tard, nous
allons continuer à travailler sur le premier but.
\begin{verbatim}
 ============================
  get key1 (update key1 (get key1 m1 - amount) m1) =
  get key1 m1 - amount

goal 2 (ID 23) is:
 key1 <> key2
\end{verbatim}
Dans le premier but, le membre droit de l'équation décrit l'interrogation d'une
base de données obtenue par mise-à-jour à la même adresse.  Ce cas de
figure est prévu l'hypothèse {\tt get\_after\_update\_same\_key}.
\begin{verbatim}
rewrite get_after_update_same_key.
\end{verbatim}
Le premier but devient donc de plus en plus simple.
\begin{verbatim}
 ============================
  get key1 m1 - amount = get key1 m1 - amount
\end{verbatim}
Ce but est maintenant une évidence logique, parce qu'il s'agit de
prouver une égalité où les deux membres sont identiques.
et il ne reste plus qu'à
le faire accepter par le système de preuve.

En fait le deuxième but est également une évidence, parce qu'il s'agit
d'une expression qui apparait directement parmi les choses que nous
avons supposées (c'est une des hypothèses du contexte).  Nous pouvons
donc demander au système de preuve de résoudre tous les cas restants.
\begin{verbatim}
all: easy.
\end{verbatim}
Le système nous répond alors qu'il n'y a plus de but.  Il faut faire
enregistrer ce succès, ce que l'on fait en appelant la commande suivante:
\begin{verbatim}
Qed.
\end{verbatim}
A partir de là, la propriété prouvée peut être utilisée pour des
preuves à venir, par exemple en l'utilisant dans une commande {\tt
  rewrite} comme nous l'avons fait pour
les hypothèses {\tt get\_after\_update\dots}

Pour compléter notre connaissance de la fonction {\tt
  abstract\_transfer}, nous convaincre de sa correction, et en
fournir une documentation complète, nous
pouvons ajouter trois autres lemmes qui auront les énoncés et les
preuves suivants:
\begin{verbatim}
Lemma get_after_abstract_transfer_when_emitter_is_receiver :
  forall (key1 key2 : string) (m1 m2: accounts) (amount : Z),
  m2 = abstract_transfer key1 key2 amount m1 ->
  (key1 = key2 -> get key1 m2 = get key1 m1).
Proof.
intros key1 key2 m1 m2 amount m2_value.
intros key1_is_key2.
rewrite m2_value.
unfold abstract_transfer.
rewrite <- key1_is_key2.
rewrite get_after_update_same_key.
rewrite get_after_update_same_key.
ring.
Qed.
\end{verbatim}
\begin{verbatim}
Lemma get_after_abstract_transfer_at_receiver :
  forall (key1 key2 : string) (m1 m2: accounts) (amount : Z),
  m2 = abstract_transfer key1 key2 amount m1 ->
  key1 <> key2 ->
  get key2 m2 = get key2 m1 + amount.
Proof.
intros key1 key2 m1 m2 amount m2_value key1_is_not_key2.
rewrite m2_value.
unfold abstract_transfer.
rewrite get_after_update_same_key.
rewrite get_after_update_different_key.
easy.
now auto.
Qed.
\end{verbatim}
\begin{verbatim}
Lemma get_after_abstract_transfer_at_different_key :
  forall (key1 key2 : string) (m1 m2: accounts) (amount : Z),
  m2 = abstract_transfer key1 key2 amount m1 ->
  forall key, key <> key1 /\ key <> key2 ->
  get key m2 = get key m1.
Proof.
intros key1 key2 m1 m2 amount m2_value key
  [key_is_not_key1 key_is_not_key2].
rewrite m2_value.
unfold abstract_transfer.
rewrite !get_after_update_different_key; easy.
Qed.
\end{verbatim}

