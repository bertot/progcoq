Require Import String List ZArith Bool.

Open Scope Z_scope.

Section abstract_program.

Variable accounts : Type.

Variable empty : accounts.

Variable update : string -> Z -> accounts -> accounts.

Variable get : string -> accounts -> Z.

Hypothesis get_after_update_same_key :
  forall m k v, get k (update k v m) = v.

Hypothesis get_after_update_different_key :
  forall m k1 k2 v, k1 <> k2 -> get k1 (update k2 v m) = get k1 m.

Definition abstract_transfer 
  (key1 key2 : string) (amount : Z) (m : accounts) :=
  let key1_balance := get key1 m in
  let intermediate := update key1 (key1_balance - amount) m in
  let key2_balance := get key2 intermediate in
  update key2 (key2_balance  + amount) intermediate.

Variable accounts1 : accounts.
Hypothesis balance_1_Alice : get "Alice" accounts1 = 10.

Let accounts2 := abstract_transfer "Alice" "Bernard" 2 accounts1.
Let accounts3 := abstract_transfer "Bernard" "Alice" 3 accounts2.

Lemma balance_3_Alice : get "Alice" accounts3 = 11.
Proof.
unfold accounts3, accounts2, abstract_transfer.
rewrite get_after_update_same_key.
rewrite get_after_update_different_key.
rewrite get_after_update_different_key.
rewrite get_after_update_same_key.
rewrite balance_1_Alice.
all:easy.
Qed.

 Lemma get_after_abstract_transfer_at_emitter :
  forall (key1 key2 : string) (m : accounts) (amount : Z),
  key1 <> key2 ->
   get key1 (abstract_transfer key1 key2 amount m) =
   get key1 m - amount.
Proof.
intros key1 key2 m1 amount.
intros key1_is_not_key2.
unfold abstract_transfer.
rewrite get_after_update_different_key.
rewrite get_after_update_same_key.
all: easy.
Qed.

Lemma get_after_abstract_transfer_when_emitter_is_receiver :
  forall (key1 key2 : string) (m1 m2: accounts) (amount : Z),
  m2 = abstract_transfer key1 key2 amount m1 ->
  (key1 = key2 -> get key1 m2 = get key1 m1).
Proof.
intros key1 key2 m1 m2 amount m2_value.
intros key1_is_key2.
rewrite m2_value.
unfold abstract_transfer.
rewrite <- key1_is_key2.
rewrite get_after_update_same_key.
rewrite get_after_update_same_key.
ring.
Qed.

Lemma get_after_abstract_transfer_at_receiver :
  forall (key1 key2 : string) (m1 m2: accounts) (amount : Z),
  m2 = abstract_transfer key1 key2 amount m1 ->
  key1 <> key2 ->
  get key2 m2 = get key2 m1 + amount.
Proof.
intros key1 key2 m1 m2 amount m2_value key1_is_not_key2.
rewrite m2_value.
unfold abstract_transfer.
rewrite get_after_update_same_key.
rewrite get_after_update_different_key.
easy.
now auto.
Qed.

Lemma get_after_abstract_transfer_at_different_key :
  forall (key1 key2 : string) (m1 m2: accounts) (amount : Z),
  m2 = abstract_transfer key1 key2 amount m1 ->
  forall key, key <> key1 /\ key <> key2 ->
  get key m2 = get key m1.
Proof.
intros key1 key2 m1 m2 amount m2_value key
  [key_is_not_key1 key_is_not_key2].
rewrite m2_value.
unfold abstract_transfer.
rewrite !get_after_update_different_key; easy.
Qed.


