\begin{center}{\bf Résumé}\end{center}
Dans cet article, nous donnons un exemple très simple de programmation
fonctionnelle et nous montrons comment ce style de programmation se
prête à des raisonnements logiques pour éviter les erreurs de
programmation.  Les raisonnements logiques peuvent eux-même être
effectués avec le système Coq.  Cet article est également une
introduction à l'utilisation de Coq, en utilisant une application
bancaire simplifiée comme illustration.
\subsection*{Une introduction à la programmation fonctionnelle}
La programmation fonctionnelle est un style de programmation où les
regroupements d'opérations sont essentiellement représentés par des
fonctions.  Ces fonctions retournent un résultat à partir d'un certain
nombre d'arguments, sans modifier de variable où de "mémoire
ambiante".

Ainsi une fonction d'addition et une fonction de multiplication
prennent chacune en entrée deux nombres et retournent un nombre,
lui-même susceptible d'être utilisé par une une opération plus tard.
Une telle fonction à deux arguments sera appelée une fonction binaire.
L'appel d'une telle fonction ne modifie pas le comportement des autres
fonctions et un nouvel appel avec les mêmes arguments retournera la
même valeur.  Bien sûr, ce genre de fonction existe aussi dans les
langages de programmation conventionnels non fonctionnels.

Ce qui distingue les langages fonctionnels, c'est que les valeurs
échangées par des fonctions peuvent également être des fonctions.
Ainsi, une fonction \(F\) pourra prendre une fonction binaire en
argument et trois nombres et appliquer répétitivement la fonction
binaire sur les arguments pour les utiliser tous.  Une telle fonction
qui prend des fonctions en argument est appelée une fonction d'ordre
supérieur.

On pourra par la suite appliquer \(F\) à la fonction d'addition pour
obtenir une fonction qui calcule la somme de trois nombres ou à la
fonction de multiplication pour obtenir une fonction qui calcule leur
produit.

Parmi les langages fonctionnels, tous utilisent cette possibilité de
passer des fonctions arguments.  Ils peuvent ensuite se différencier
suivant qu'ils adhèrent à une discipline de programmation
fonctionnelle avec effets de bord ou de programmation fonctionnelle
pure.  Pour la programmation fonctionnelle avec effets de bord, on
inclut également dans le langage de programmation des primitives
permettant de modifier des variables existantes ou un état global.  Le
langage OCaml est un exemple de langage de programmation avec effets
de bord.  Dans la programmation fonctionnelle pure, ces primitives
sont inexistantes.  Haskell est un exemple de langage de programmation
fonctionnelle pure.  L'avantage de la programmation fonctionnelle pure
est que l'on sait dès la
définition d'une variable et pour toute sa durée de vie quelle est la
valeur de cette variable.

La programmation fonctionnelle pure facilite le raisonnement sur les
programmes.  Si l'on établit à un moment donné la valeur d'une
fonction pour une certaine entrée, cette valeur est établie une fois
pour toutes.  En programmation avec effets de bord, toute propriété
faisant référence à des variables du programme a une validité limitée
dans le temps.

Les calculs que l'on peut faire en programmation fonctionnelle pure
sont les mêmes que ceux que l'on peut faire en programmation avec
effets de bord, mais l'efficacité est souvent moins bonne.  Une
opération de mise-à-jour dans un tableau a un coût constant dans un
langage à effet de bord, mais dans un programme fonctionnel pur, ce
coût est en moyenne logarithmique vis-à-vis de la taille du tableau.
Pour cette raison, la programmation fonctionnelle pure est parfois
évitée dans le développement de programmes efficaces.

Les cas d'usage de la programmation fonctionnelle sont très variés et
touchent souvent des domaines où le coût des erreurs serait
catastrophique: applications bancaires, blockchains, trading à haute
fréquence.  Dans ces domaines, un avantage souvent cité est la
grande lisibilité par des experts du domaine d'application
(financiers, traders) tout en permettant une programmation très sûre.
D'autres exemples d'application concernent
des outils de programmation comme des compilateurs ou des analyseurs
pour d'autres langages de programmation, comme le langage C
(compilateur CompCert) ou des outils de preuve et de vérification de
programmes, comme le langage Coq ou FramaC.  Pour ces applications,
c'est la facilité à programmer sans erreur avec de nouveaux types de
données qui est souvent appréciée.

Lorsque l'on s'intéresse à la correction des programmes, la
programmation fonctionnelle pure est un outil puissant.  D'une part,
il existe des programmes pour lesquels l'efficacité des programmes
fonctionnels pur suffit.  Mais même lorsque
le programme étudié est un programme à effets de bords, il est
possible d'utiliser un programme fonctionnel pur comme étape
intermédiaire pour vérifier que le programme étudié est correct.  Dans
une première étape, on montre que les calculs effectués dans le
programme étudié sont correctement modélisés par le programme
fonctionnel (même si les deux programmes ne prennent pas le même temps
d'exécution).  Dans un second temps, on montre que le programme
fonctionnel pur satisfait les propriétés désirées.
Cette approche permet de vérifier par exemple qu'une relation est bien
satisfaite entre l'état initial et l'état final du programme étudié,
ou bien qu'une certaine propriété est satisfaite dans l'état final.

Dans cette première partie, nous ne montrerons pas comment établir une
correspondance entre programmes à effets de bords et programmes en
programmation fonctionnelle pure.  Nous nous concentrons sur la
preuve de propriétés pour ces derniers.
